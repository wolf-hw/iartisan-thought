package org.iartisan.thought.web.controller.management;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.utils.StringUtils;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.req.CommentAddReq;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.CommentManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 回复信息管理
 *
 * @author King
 * @since 2018/1/3
 */
@Controller
@RequestMapping("comment/management")
public class CommentManagementController extends BaseController {

    @Autowired
    private CommentManagementClient commentManagementClient;

    @PostMapping(ReqContants.REQ_ADD_DATA + "/{topicId}")
    @ResponseBody
    public WebR addData(@PathVariable String topicId, String content) {
        CommentAddReq req = new CommentAddReq();
        req.setTopicId(topicId);
        req.setContent(content);
        req.setCustId(getCustId());
        WebR webR = new WebR();
        try {
            commentManagementClient.addComment(req);
            webR.setMessage("/topic/query/queryDetailData/" + topicId);
        } catch (BizRemoteException e) {
            webR.isError(e.getMessage());
        }
        return webR;
    }

    @PostMapping("accept")
    @ResponseBody
    public WebR accept(String commentId) {
        WebR webR = new WebR();
        try {
            commentManagementClient.acceptById(commentId);
        } catch (BizRemoteException e) {
            webR.isError(e.getMessage());
        }
        return webR;
    }

    @PostMapping("zan")
    @ResponseBody
    public WebR zan(String commentId) {
        WebR webR = new WebR();
        try {
            String custId = getCustId();
            if (StringUtils.isEmpty(custId)) {
                webR.isError("未登录");
                return webR;
            }
            commentManagementClient.addZan(commentId, custId);
        } catch (BizRemoteException e) {
            webR.isError(e.getMessage());
        }
        return webR;
    }
}
