package org.iartisan.thought.web.controller.my;

import org.iartisan.runtime.web.WebR;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.CustManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 个人中心设置
 *
 * @author King
 * @since 2017/12/19
 */
@Controller
@RequestMapping("my")
public class MyManagementController extends BaseController {


    @Autowired
    private CustManagementClient custManagementClient;

    @PostMapping(value = "setAvatar")
    public WebR setAvatar(String avatar) {
        WebR r = new WebR();
        try {
            custManagementClient.setAvatar(getCustId(), avatar);
        } catch (Exception e) {
            r.isError(e.getMessage());
        }
        return r;
    }
}
