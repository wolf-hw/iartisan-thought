package org.iartisan.thought.web.controller.management;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.WarmChannelManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author King
 * @since 2018/1/30
 */
@Controller
@RequestMapping("warmChannel/management")
public class WarmChannelManagementController extends BaseController {

    @Autowired
    private WarmChannelManagementClient warmChannelManagementClient;

    @PostMapping(ReqContants.REQ_ADD_DATA)
    @ResponseBody
    public WebR addData(String topicId) {
        WebR r = new WebR();
        try {
            warmChannelManagementClient.setWarmChannel(topicId);
        } catch (BizRemoteException e) {
            r.isError(e.getMessage());
        }
        return r;
    }

}
