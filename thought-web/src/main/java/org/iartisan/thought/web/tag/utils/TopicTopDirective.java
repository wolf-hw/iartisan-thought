package org.iartisan.thought.web.tag.utils;

import java.io.IOException;
import java.io.Writer;
import java.text.MessageFormat;

/**
 * <p>
 * datetime
 *
 * @author King
 * @since 2017/12/21
 */
public class TopicTopDirective extends AdminTopicDirective {

    private static String pattern = "<span class=\"layui-badge layui-bg-black\">{0}</span>";

    private static String admin_pattern = "<span class=\"layui-btn layui-btn-xs jie-admin\" type=\"top\">{0}</span>";

    protected static String actionYesText = "置顶";

    protected static String actionNoText = "取消置顶";

    protected static String text = "置顶";

    public static void render(Writer writer, String value) throws IOException {
        AdminTopicDirective.render(writer, value, pattern, admin_pattern, actionYesText, actionNoText, text);
    }
}
