package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.res.BaseCheckRes;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.thought.server.api.query.CustTopicCollectQueryFacade;
import org.iartisan.thought.server.api.req.TopicCollectReq;
import org.iartisan.thought.server.api.res.TopicBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust topic collect query client
 *
 * @author King
 * @since 2018/1/12
 */
@Service
public class CustTopicCollectQueryClient {

    @Autowired
    private CustTopicCollectQueryFacade custTopicCollectQueryFacade;

    public boolean isCollect(String custId, String topicId) {
        TopicCollectReq req = new TopicCollectReq();
        req.setCustId(custId);
        req.setTopicId(topicId);
        BaseCheckRes res = custTopicCollectQueryFacade.checkTopicCollect(req);
        return res.isResult();
    }

    public PageWrapper<TopicBean> getMyCollectTopicPageData(String custId, Page page) {
        TopicCollectReq req = new TopicCollectReq();
        req.setCustId(custId);
        BasePageRes<TopicBean> res = custTopicCollectQueryFacade.getMyCollectTopicPageData(req, page);
        return res.getDataPage();
    }
}
