package org.iartisan.thought.web.controller.query;

import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.query.CustQueryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author King
 * @since 2018/1/17
 */
@Controller
@RequestMapping("user/query")
public class CustQueryController extends BaseController {

    @Autowired
    private CustQueryClient custQueryClient;

    @GetMapping(value = "{userId}")
    public String home(Model model, @PathVariable String userId) {
        model.addAttribute(data, custQueryClient.getCustInfoByCustId(userId));
        return "my/my_home";
    }
}
