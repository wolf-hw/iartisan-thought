package org.iartisan.thought.web.controller;

import org.apache.shiro.SecurityUtils;
import org.iartisan.runtime.web.authentication.RealmBean;
import org.iartisan.runtime.web.contants.WebConstants;
import org.iartisan.runtime.web.utils.WebUtil;
import org.iartisan.thought.web.integration.query.BasicQueryClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * <p>
 * base
 *
 * @author King
 * @since 2017/12/19
 */
public abstract class BaseController {

    protected final static String data = "data";

    protected final static String _categories = "_categories";

    protected final static String _topicPageData = "_topicPageData";

    protected final static String _categoryKey = "_categoryKey";

    protected final static String _statusKey = "_statusKey";

    protected final static String _redirect = "redirect:";

    protected Logger logger = LoggerFactory.getLogger(getClass());

    //protected final static String alert_success_msg = "操作成功";

    @Autowired
    private BasicQueryClient basicQueryClient;

    protected String getCustId() {
        RealmBean realmBean = (RealmBean) WebUtil.getShiroSession().getAttribute(WebConstants._USER);
        if (null != realmBean) {
            return realmBean.getUserId();
        }
        return "";
    }

    protected void setCategories() {
        Object object = WebUtil.getShiroSession().getAttribute(_categories);
        if (null == object) {
            WebUtil.getShiroSession().setAttribute(_categories, basicQueryClient.getCategories());
        }
    }

}
