package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.query.WarmChannelQueryFacade;
import org.iartisan.thought.server.api.res.WarmChannelBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author King
 * @since 2018/1/30
 */
@Service
public class WarmChannelQueryClient {

    @Autowired
    private WarmChannelQueryFacade warmChannelQueryFacade;

    public List<WarmChannelBean> getWarmChannels() throws BizRemoteException {
        return ApiResUtil.getDataList(warmChannelQueryFacade.getWarmChannels(new BaseReq()));
    }
}
