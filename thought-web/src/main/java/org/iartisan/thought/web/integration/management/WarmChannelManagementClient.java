package org.iartisan.thought.web.integration.management;

import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.management.WarmChannelManagementFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/1/30
 */
@Service
public class WarmChannelManagementClient {

    @Autowired
    private WarmChannelManagementFacade warmChannelManagementFacade;

    public void setWarmChannel(String topicId) throws BizRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setTopicId(topicId);
        ApiResUtil.getResult(warmChannelManagementFacade.setWarmChannel(req));
    }
}
