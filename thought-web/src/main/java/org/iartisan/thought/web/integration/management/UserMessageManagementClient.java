package org.iartisan.thought.web.integration.management;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.management.UserMessageManagementFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.req.BaseIdReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/2/7
 */
@Service
public class UserMessageManagementClient {

    @Autowired
    private UserMessageManagementFacade userMessageManagementFacade;

    /**
     * 删除消息
     *
     * @return
     */
    public void deleteByCustId(String custId) throws BizRemoteException {
        BaseCustReq req = new BaseCustReq();
        req.setCustId(custId);
        ApiResUtil.getResult(userMessageManagementFacade.deleteByCustId(req));
    }

    /**
     * 删除消息
     *
     * @return
     */
    public void deleteById(Integer id) throws BizRemoteException {
        BaseIdReq req = new BaseIdReq();
        req.setId(id);
        ApiResUtil.getResult(userMessageManagementFacade.deleteById(req));
    }
}
