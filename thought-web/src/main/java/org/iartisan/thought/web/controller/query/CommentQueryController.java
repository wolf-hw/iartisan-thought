package org.iartisan.thought.web.controller.query;

import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.res.CommentBean;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.query.CommentQueryClient;
import org.iartisan.thought.web.vo.CommentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 回复信息查询
 *
 * @author King
 * @since 2018/1/3
 */
@Controller
@RequestMapping("comment/query")
public class CommentQueryController extends BaseController {

    @Autowired
    private CommentQueryClient commentQueryClient;

    @ResponseBody
    @PostMapping(ReqContants.REQ_QUERY_LIST_DATA)
    public WebR queryListData(String topicId, Page page) throws BizRemoteException {
        PageWrapper<CommentVO> pageWrapper = commentQueryClient.getCommentPageData(page, topicId, getCustId());
        WebR r = new WebR(pageWrapper.getPage());
        r.setDataList(pageWrapper.getDataList());
        return r;
    }
}
