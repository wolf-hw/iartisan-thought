package org.iartisan.thought.web.controller;

import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.req.TopicQueryReq;
import org.iartisan.thought.web.integration.query.TopicQueryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>
 * index
 *
 * @author King
 * @since 2017/12/7
 */
@Controller
public class IndexController extends BaseController {

    @Autowired
    private TopicQueryClient topicQueryClient;


    @RequestMapping(value = "index", method = {RequestMethod.GET, RequestMethod.POST})
    public String index(Model model) throws BizRemoteException {
        init(model);
        return "/topic/topic_index";
    }

    @RequestMapping(value = "", method = {RequestMethod.GET, RequestMethod.POST})
    public String home(Model model) throws BizRemoteException {
        init(model);
        return "/topic/topic_index";
    }

    public void init(Model model) throws BizRemoteException {
        //获取所有贴子
        TopicQueryReq req = new TopicQueryReq();
        req.setIsTop("N");
        model.addAttribute(_topicPageData, topicQueryClient.getTopicPageData(req, new Page()));
        //获取置顶贴
        model.addAttribute("topTopics", topicQueryClient.getTopTopicList());
        model.addAttribute(_categoryKey, "all");
        model.addAttribute(_statusKey, "all");
        setCategories();
    }

}
