package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.thought.server.api.query.BasicQueryFacade;
import org.iartisan.thought.server.api.res.CategoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * <p>
 * basic query client
 * @author King
 * @since 2017/12/19
 */
@Service
public class BasicQueryClient {

    @Autowired
    private BasicQueryFacade basicQueryFacade;

    public List<CategoryBean> getCategories() {
        List<CategoryBean> list = basicQueryFacade.getCategoryList(new BaseReq()).getDataList();
        return list;
    }
}
