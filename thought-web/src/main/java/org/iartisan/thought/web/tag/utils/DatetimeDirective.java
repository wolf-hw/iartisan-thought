package org.iartisan.thought.web.tag.utils;

import org.iartisan.runtime.utils.RelativeDateFormatUtil;

import java.io.IOException;
import java.io.Writer;

/**
 * <p>
 * datetime
 *
 * @author King
 * @since 2017/12/21
 */
public class DatetimeDirective {

    public static void render(Writer writer, String value) throws IOException {
        String relative = RelativeDateFormatUtil.format(value);
        writer.write(relative);
    }

}
