package org.iartisan.thought.web.integration.management;

import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.management.CommentManagementFacade;
import org.iartisan.thought.server.api.req.BaseCommentReq;
import org.iartisan.thought.server.api.req.CommentAddReq;
import org.iartisan.thought.server.api.req.CommentZanReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * comment management client impl
 *
 * @author King
 * @since 2018/1/3
 */
@Service
public class CommentManagementClient {

    @Autowired
    private CommentManagementFacade commentManagementFacade;


    public void addComment(CommentAddReq req) throws BizRemoteException {
        ApiResUtil.getResult(commentManagementFacade.addCommentData(req));
    }

    public void acceptById(String id) throws BizRemoteException {
        BaseCommentReq req = new BaseCommentReq();
        req.setCommentId(id);
        ApiResUtil.getResult(commentManagementFacade.accept(req));
    }

    public void addZan(String commentId, String custId) throws BizRemoteException {
        CommentZanReq req = new CommentZanReq();
        req.setCommentId(commentId);
        req.setCustId(custId);
        ApiResUtil.getResult(commentManagementFacade.addZan(req));
    }
}
