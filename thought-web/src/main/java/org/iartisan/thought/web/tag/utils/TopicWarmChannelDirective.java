package org.iartisan.thought.web.tag.utils;

import java.io.IOException;
import java.io.Writer;

/**
 * <p>
 * warm channel
 *
 * @author King
 * @since 2017/12/21
 */
public class TopicWarmChannelDirective extends AdminTopicDirective {

    private static String pattern = "<span class=\"layui-badge layui-bg-blue\">{0}</span>";

    private static String admin_pattern = "<span class=\"layui-badge layui-bg-blue jie-admin\" type=\"channel\">{0}</span>";

    protected static String actionYesText = "加入温馨通道";

    protected static String actionNoText = "移除温馨通道";

    protected static String text = "温馨通道";

    public static void render(Writer writer, String value) throws IOException {
        AdminTopicDirective.render(writer, value, pattern, admin_pattern, actionYesText, actionNoText, text);
    }
}
