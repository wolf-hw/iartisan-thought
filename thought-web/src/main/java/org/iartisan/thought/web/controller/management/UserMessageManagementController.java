package org.iartisan.thought.web.controller.management;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.UserMessageManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author King
 * @since 2018/2/7
 */
@Controller
@RequestMapping("userMessage/management")
public class UserMessageManagementController extends BaseController {

    @Autowired
    private UserMessageManagementClient userMessageManagementClient;

    @ResponseBody
    @PostMapping(ReqContants.REQ_DELETE_DATA)
    public WebR deleteData(Integer id) {
        WebR r = new WebR();
        try {
            userMessageManagementClient.deleteById(id);
        } catch (BizRemoteException e) {
            r.isError(e.getMessage());
        }
        return r;
    }

    @ResponseBody
    @GetMapping("deleteByCustId")
    public WebR deleteByCustId() {
        WebR r = new WebR();
        try {
            userMessageManagementClient.deleteByCustId(getCustId());
        } catch (BizRemoteException e) {
            r.isError(e.getMessage());
        }
        return r;
    }
}
