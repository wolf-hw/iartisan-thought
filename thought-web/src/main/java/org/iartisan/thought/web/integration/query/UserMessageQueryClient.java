package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.query.UserMessageQueryFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.res.UserMessageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * user消息
 *
 * @author King
 * @since 2018/2/6
 */
@Service
public class UserMessageQueryClient {

    @Autowired
    private UserMessageQueryFacade userMessageQueryFacade;

    public Integer getUnreadMsgCount(String custId) throws BizRemoteException {
        BaseCustReq req = new BaseCustReq();
        req.setCustId(custId);
        BaseOneRes<Integer> res = userMessageQueryFacade.getUnreadMsgCount(req);
        return ApiResUtil.getBean(res);
    }

    public List<UserMessageBean> getUnreadMsgs(String custId) throws BizRemoteException {
        BaseCustReq req = new BaseCustReq();
        req.setCustId(custId);
        BaseListRes<UserMessageBean> res = userMessageQueryFacade.getUnreadMsgs(req);
        return ApiResUtil.getDataList(res);
    }
}
