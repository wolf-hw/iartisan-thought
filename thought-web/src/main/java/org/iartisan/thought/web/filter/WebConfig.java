package org.iartisan.thought.web.filter;


import org.iartisan.runtime.env.EnvContextConfig;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

@Configuration
@Order
public class WebConfig extends WebMvcConfigurerAdapter implements ServletContextInitializer {

    @Bean
    public EmbeddedServletContainerCustomizer containerCustomizer() {

        return (container -> {
            ErrorPage error404Page = new ErrorPage(HttpStatus.NOT_FOUND, "/404");
            ErrorPage error500Page = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500");
            container.addErrorPages(error404Page, error500Page);
        });

    }

    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        servletContext.setAttribute("_baidutongji", EnvContextConfig.get("baidu.tongji.script"));
    }
}
