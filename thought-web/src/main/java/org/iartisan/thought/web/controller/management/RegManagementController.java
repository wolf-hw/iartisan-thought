package org.iartisan.thought.web.controller.management;

import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.web.integration.management.CustManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * <p>
 * req controller
 *
 * @author King
 * @since 2017/12/15
 */
@Controller
@RequestMapping("reg")
public class RegManagementController {

    @Autowired
    private CustManagementClient custManagementClient;

    @GetMapping(ReqContants.REQ_ADD_DATA_PAGE)
    public String addDataPage() {
        return "reg";
    }

    @RequestMapping(value = ReqContants.REQ_ADD_DATA, method = {RequestMethod.GET, RequestMethod.POST})
    public String addData(CustCipAddReq req) {
        custManagementClient.addCustCip(req);
        return "reg";
    }


}
