package org.iartisan.thought.web.integration.management;


import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.runtime.api.utils.ApiResUtil;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.thought.server.api.management.TopicManagementFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.TopicAddReq;
import org.iartisan.thought.server.api.req.TopicCollectReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 贴子服务
 *
 * @author King
 * @since 2017/12/11
 */
@Service
public class TopicManagementClient {

    @Autowired
    private TopicManagementFacade topicManagementFacade;


    public void addTopic(TopicAddReq req) throws BizRemoteException {
        BaseRes res = topicManagementFacade.addTopic(req);
        ApiResUtil.getResult(res);
    }

    public void addReadcount(String topicId) throws BizRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setTopicId(topicId);
        BaseRes res = topicManagementFacade.addReadcount(req);
        ApiResUtil.getResult(res);
    }

    public void addCollectTopic(String custId, String topicId) throws BizRemoteException {
        TopicCollectReq req = new TopicCollectReq();
        req.setCustId(custId);
        req.setTopicId(topicId);
        BaseRes res = topicManagementFacade.addCollectTopic(req);
        ApiResUtil.getResult(res);
    }

    public void deleteTopic(String topicId) throws BizRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setTopicId(topicId);
        BaseRes res = topicManagementFacade.deleteTopic(req);
        ApiResUtil.getResult(res);
    }

    public void setTopicTop(String topicId) throws BizRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setTopicId(topicId);
        BaseRes res = topicManagementFacade.setTopicTop(req);
        ApiResUtil.getResult(res);
    }

    public void setTopicFine(String topicId) throws BizRemoteException {
        BaseTopicReq req = new BaseTopicReq();
        req.setTopicId(topicId);
        BaseRes res = topicManagementFacade.setTopicFine(req);
        ApiResUtil.getResult(res);
    }
}
