package org.iartisan.thought.web.controller.management;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.req.TopicAddReq;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.TopicManagementClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * topic management
 *
 * @author King
 * @since 2017/12/13
 */
@Controller
@RequestMapping("topic/management")
public class TopicManagementController extends BaseController {

    @Autowired
    private TopicManagementClient topicManagementClient;

    private static final String prefix = "topic/";

    @GetMapping(ReqContants.REQ_ADD_DATA_PAGE)
    public String addDataPage() {
        setCategories();
        return prefix + "topic_add";
    }

    @PostMapping(ReqContants.REQ_ADD_DATA)
    @ResponseBody
    public WebR addData(TopicAddReq req) {
        WebR r = new WebR();
        //验证校验码
        req.setCustId(getCustId());
        try {
            topicManagementClient.addTopic(req);
            r.setMessage("/index");
        } catch (BizRemoteException e) {
            r.isError(e.getMessage());
        }
        return r;
    }

    //贴子收藏功能
    @PostMapping("collect")
    @ResponseBody
    public WebR collect(String topicId) {
        WebR r = new WebR();
        try {
            topicManagementClient.addCollectTopic(getCustId(), topicId);
            r.setMessage("收藏成功");
        } catch (BizRemoteException e) {
            r.isError(e.getMessage());
        }
        //验证校验码
        return r;
    }

    //贴子收藏功能
    @PostMapping("action")
    @ResponseBody
    public WebR action(String topicId, String action) {
        WebR r = new WebR();
        try {
            //删除、置顶、加精
            if (action.equals("del")) {
                topicManagementClient.deleteTopic(topicId);
            }
            if (action.equals("fine")) {
                topicManagementClient.setTopicFine(topicId);
            }
            if (action.equals("top")) {
                topicManagementClient.setTopicTop(topicId);
            }
        } catch (BizRemoteException e) {
            r.isError(e.getMessage());
        }
        return r;
    }
}
