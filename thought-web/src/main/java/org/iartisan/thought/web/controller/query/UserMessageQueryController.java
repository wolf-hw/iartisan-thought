package org.iartisan.thought.web.controller.query;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.res.WarmChannelBean;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.query.UserMessageQueryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @author King
 * @since 2018/2/6
 */
@Controller
@RequestMapping("userMessage/query")
public class UserMessageQueryController extends BaseController {

    @Autowired
    private UserMessageQueryClient userMessageQueryClient;

    @PostMapping("getUnreadCount")
    @ResponseBody
    public WebR queryListData() {
        WebR r = new WebR();
        try {
            Integer count = userMessageQueryClient.getUnreadMsgCount(getCustId());
            r.setDataObject(count);
        } catch (BizRemoteException e) {
            r.setCode("0");
        }
        return r;
    }
}
