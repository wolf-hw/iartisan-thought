package org.iartisan.thought.web.controller.query;

import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.utils.StringUtils;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.req.TopicQueryReq;
import org.iartisan.thought.server.api.res.TopicBean;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.TopicManagementClient;
import org.iartisan.thought.web.integration.query.CommentQueryClient;
import org.iartisan.thought.web.integration.query.CustTopicCollectQueryClient;
import org.iartisan.thought.web.integration.query.TopicQueryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 * topic query
 *
 * @author King
 * @since 2017/12/13
 */
@Controller
@RequestMapping("topic/query")
public class TopicQueryController extends BaseController {

    @Autowired
    private TopicQueryClient topicQueryClient;

    @Autowired
    private TopicManagementClient topicManagementClient;

    @Autowired
    private CommentQueryClient commentQueryClient;

    @Autowired
    private CustTopicCollectQueryClient custTopicCollectQueryClient;

    private static final String prefix = "topic/";

    @GetMapping(ReqContants.REQ_QUERY_DETAIL_DATA + "/{topicId}")
    public String queryDetailData(@PathVariable String topicId, Page page, Model model) throws BizRemoteException {
        try {
            model.addAttribute(data, topicQueryClient.getTopicDetailById(topicId));
            topicManagementClient.addReadcount(topicId);
        } catch (BizRemoteException e) {
            logger.error("RemoteException:", e);
        }
        //查询备注信息
        model.addAttribute("comments", commentQueryClient.getCommentPageData(page, topicId, getCustId()));
        if (StringUtils.isNotEmpty(getCustId())) {
            model.addAttribute("collect", custTopicCollectQueryClient.isCollect(getCustId(), topicId));
        }
        //判断用户是否收藏了该贴
        return prefix + "topic_detail";
    }

    @GetMapping(ReqContants.REQ_QUERY_PAGE_DATA + "/{categoryKey}/{statusKey}")
    public String queryPageData(@PathVariable String categoryKey,
                                @PathVariable String statusKey,
                                Page page,
                                Model model) throws BizRemoteException {
        model.addAttribute(_categoryKey, categoryKey);
        model.addAttribute(_statusKey, statusKey);
        TopicQueryReq req = new TopicQueryReq();
        if (StringUtils.isNotEmpty(statusKey) && !statusKey.equals("all")) {
            req.setIsEnd(statusKey);
        }
        if (StringUtils.isNotEmpty(categoryKey) && !categoryKey.equals("all")) {
            req.setCategoryKey(categoryKey);
        }
        model.addAttribute(_topicPageData, topicQueryClient.getTopicPageData(req, page));
        return prefix + "topic_all_index";
    }

    @GetMapping("queryMyPageData")
    @ResponseBody
    public WebR queryMyPageData(Page page) {
        WebR webR = null;
        try {
            PageWrapper<TopicBean> pageData = topicQueryClient.getMyTopicPageData(page, getCustId());
            webR = new WebR(pageData.getPage());
            webR.setDataList(pageData.getDataList());
        } catch (BizRemoteException e) {
            e.printStackTrace();
        }
        return webR;
    }

    @GetMapping("queryMyCollectPageData")
    @ResponseBody
    public WebR queryMyCollectPageData(Page page) {
        PageWrapper<TopicBean> pageData = custTopicCollectQueryClient.getMyCollectTopicPageData(getCustId(), page);
        WebR webR = new WebR(pageData.getPage());
        webR.setDataList(pageData.getDataList());
        return webR;
    }

    @PostMapping("getHotTopics")
    @ResponseBody
    public WebR getHotTopics() {
        WebR r = new WebR();
        try {
            List<TopicBean> dataList = topicQueryClient.getHotTopicList();
            r.setDataList(dataList);
        } catch (BizRemoteException e) {
            r.setCode("11");
        }
        return r;
    }
}
