package org.iartisan.thought.web.controller.my;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.runtime.web.utils.WebUtil;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.query.CustQueryClient;
import org.iartisan.thought.web.integration.query.UserMessageQueryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * <p>
 * 个人中心直询
 *
 * @author King
 * @since 2017/12/19
 */
@Controller
@RequestMapping("my")
public class MyQueryController extends BaseController {

    private final static String prefix = "my/";

    private static final String active_code = "active_code";

    private static final String tab_code = "tab_code";

    @Autowired
    private UserMessageQueryClient userMessageQueryClient;

    @GetMapping(value = ReqContants.REQ_INDEX)
    public String index(Model model, String code) {
        model.addAttribute(active_code, "my_index");
        model.addAttribute(tab_code, code);
        return prefix + "my_index";
    }

    @GetMapping(value = "message")
    public String message(Model model) throws BizRemoteException {
        model.addAttribute(active_code, "my_message");
        model.addAttribute(data, userMessageQueryClient.getUnreadMsgs(getCustId()));
        return prefix + "my_message";
    }

    @Autowired
    private CustQueryClient custQueryClient;

    @GetMapping(value = "set")
    public String set(Model model) {
        //查询用户基本信息
        model.addAttribute(data, custQueryClient.getCustInfoByCustId(getCustId()));
        model.addAttribute(active_code, "my_set");
        return prefix + "my_set";
    }

    @GetMapping(value = "home")
    public String home(Model model) {
        model.addAttribute(active_code, "my_home");
        model.addAttribute(data, custQueryClient.getCustInfoByCustId(getCustId()));
        return prefix + "my_home";
    }
}
