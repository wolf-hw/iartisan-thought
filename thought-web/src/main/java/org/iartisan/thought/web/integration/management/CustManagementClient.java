package org.iartisan.thought.web.integration.management;

import org.iartisan.thought.server.api.management.CustManagementFacade;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.server.api.req.CustModifyReq;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust management client impl
 *
 * @author King
 * @since 2017/12/16
 */
@Service
public class CustManagementClient {

    @Autowired
    private CustManagementFacade custManagementFacade;


    public void addCustCip(CustCipAddReq req) {
        custManagementFacade.addCustData(req);
    }


    public void setAvatar(String custId, String avatar) {
        CustModifyReq req = new CustModifyReq();
        req.setCustId(custId);
        req.setAvatar(avatar);
        custManagementFacade.setCustAvatar(req);
    }
}
