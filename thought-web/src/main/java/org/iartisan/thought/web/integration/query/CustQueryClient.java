package org.iartisan.thought.web.integration.query;

import org.iartisan.runtime.api.code.ResCodeEnum;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.thought.server.api.query.CustQueryFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.res.CustBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust query client
 *
 * @author King
 * @since 2018/1/5
 */
@Service
public class CustQueryClient {

    @Autowired
    private CustQueryFacade custQueryFacade;

    public CustBean getCustInfoByCustId(String custId) {
        BaseCustReq req = new BaseCustReq();
        req.setCustId(custId);
        BaseOneRes<CustBean> res = custQueryFacade.getCustByCustId(req);
        if (res.getCode().equals(ResCodeEnum.succcess.getCode())) {
            return res.getDataObject();
        }
        return null;
    }
}
