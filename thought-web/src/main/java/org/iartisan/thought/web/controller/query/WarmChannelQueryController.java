package org.iartisan.thought.web.controller.query;

import org.iartisan.runtime.exception.BizRemoteException;
import org.iartisan.runtime.web.WebR;
import org.iartisan.runtime.web.contants.ReqContants;
import org.iartisan.thought.server.api.res.WarmChannelBean;
import org.iartisan.thought.web.controller.BaseController;
import org.iartisan.thought.web.integration.management.WarmChannelManagementClient;
import org.iartisan.thought.web.integration.query.WarmChannelQueryClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author King
 * @since 2018/1/30
 */
@Controller
@RequestMapping("warmChannel/query")
public class WarmChannelQueryController extends BaseController {

    @Autowired
    private WarmChannelQueryClient warmChannelQueryClient;

    @PostMapping(ReqContants.REQ_QUERY_LIST_DATA)
    @ResponseBody
    public WebR queryListData() {
        WebR r = new WebR();
        try {
            List<WarmChannelBean> dataList = warmChannelQueryClient.getWarmChannels();
            r.setDataList(dataList);
        } catch (BizRemoteException e) {
            r.setCode("0");
        }
        return r;
    }

}
