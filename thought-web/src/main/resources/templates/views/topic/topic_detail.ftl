<title>${data.title!''}</title>
<div class="layui-row layui-col-space15">
    <div class="layui-col-md8 content detail">
        <div class="fly-panel detail-box">
            <h1>${data.title!''}</h1>
            <div class="fly-detail-info" data-id="${data.id}">
                <span class="layui-badge layui-bg-green fly-detail-column">${data.categoryName}</span>
                <span class="layui-badge" style="background-color: #999;" id="topidIsEnd">
                <@tagcode code="topic_end" value="${data.isEnd}"> </@tagcode>
                </span>
            <@tagcode code="topic_top"  value="${data.isTop}|${_user.role}"> </@tagcode>
            <@tagcode code="topic_fine" value="${data.isFine}|${_user.role}"> </@tagcode>
            <#if (_user.role) ?? >
                <#if (_user.role)='admin'>
                    <span class="layui-badge layui-bg-orange jie-admin" type="del">删除</span>
                </#if>
            </#if>
            <@tagcode code="topic_warm_channel" value="${data.isWarmChannel}|${_user.role}"> </@tagcode>
                <span class="fly-list-nums">
                    <a href="#comment"><i class="iconfont" title="回答">&#xe60c;</i>${data.commentCount!0}</a>
                    <i class="iconfont" title="人气">&#xe60b;</i>${data.readCount!0}
                </span>
            </div>
            <div class="detail-about">
                <a class="fly-avatar" href="${context.contextPath}/user/query/${data.customerId}">
                <#if (data.avatar) ??>
                    <img src="${context.contextPath}/oss/rest/download/${data.avatar}"
                         alt="${data.custNickname}">
                <#else>
                    <img src="/assets/images/avatar/default.png" alt="${data.custNickname}">
                </#if>
                </a>
                <div class="fly-detail-user">
                    <a href="" class="fly-link">
                        <cite>${topTopic.custNickname}</cite>
                    <#--  <i class="iconfont icon-renzheng" title="认证信息：{{ rows.user.approve }}"></i>
                      <i class="layui-badge fly-badge-vip">VIP3</i>-->
                    </a>
                    <span>${data.createTime?date}</span>
                </div>
                <div class="detail-hits" id="LAY_jieAdmin" data-id="${data.id!''}">
                    <span style="padding-right: 10px; color: #FF7200">悬赏：60 积分</span>
                <#if collect ?? >
                    <span class="layui-btn-warm layui-btn-radius  layui-btn-xs jie-admin" type="edit">
                        <a id="collect" href="javascript:;;" data-id="${data.id!''}">
                            <#if collect=true >
                                取消收藏
                            <#else>
                                收藏
                            </#if>
                        </a>
                    </span>
                </#if>
                </div>
            </div>
            <div class="detail-body photos">
            ${data.content!''}
            </div>
        </div>

        <div class="fly-panel detail-box" id="flyReply">
            <fieldset class="layui-elem-field layui-field-title" style="text-align: center;">
                <legend>回帖</legend>
            </fieldset>
            <ul class="jieda" id="jieda">
            <#if comments ?? >
                <#if (comments.dataList) ??>
                    <#list comments.dataList as comment>
                        <li data-id="${comment.id}" class="jieda-daan">
                            <a name="item-${comment.id}"></a>
                            <div class="detail-about detail-about-reply">
                                <a class="fly-avatar" href="">
                                    <#if (comment.avatar) ??>
                                        <img src="${context.contextPath}/oss/rest/download/${comment.avatar}">
                                    <#else>
                                        <img src="/assets/images/avatar/default.png">
                                    </#if>
                                </a>
                                <div class="fly-detail-user">
                                    <a href="" class="fly-link">
                                        <cite>${comment.custNickName}</cite>
                                    <#--<i class="iconfont icon-renzheng" title="认证信息：{{ item.user.approve }}"></i>
                                    <i class="layui-badge fly-badge-vip">VIP3</i>-->
                                    </a>
                                <#-- <span>(楼主)</span>-->
                                </div>
                                <div class="detail-hits">
                                    <span>${comment.createTime?date}</span>
                                </div>
                                <#if (comment.isAccept) ??>
                                    <#if (comment.isAccept)='Y'>
                                        <i class="iconfont icon-caina" title="最佳答案"></i>
                                    </#if>
                                </#if>
                            </div>
                            <div class="detail-body jieda-body photos">
                                <p>${comment.content}</p>
                            </div>
                            <div class="jieda-reply">
                              <span class="jieda-zan" type="zan">
                                <i class="iconfont icon-zan <#if (comment.zaned) ??><#if (comment.zaned) >zanok</#if></#if>"></i>
                                <em>${comment.zanCount!'0'}</em>
                              </span>
                                <span type="reply"><i class="iconfont icon-svgmoban53"></i>回复</span>
                                <div class="jieda-admin">
                                    <!-- 登录人 如果是发贴人 贴子如果没有被采纳过 -->
                                    <#if (data.canAccept) ?? >
                                        <#if (data.canAccept) ='Y' >
                                            <#if _user ?? >
                                                <#if (_user.userId)=(data.customerId)>
                                                    <span class="jieda-accept" type="accept">采纳</span>
                                                </#if>
                                            </#if>
                                        </#if>
                                    </#if>
                                </div>
                            </div>
                        </li>
                    </#list>
                    <#if (comments.page) ?? >
                        <div style="text-align: center">
                            <div class="laypage-main"><@pageCode currPage="${(comments.page).currPage!''}" pageSize="${(comments.page).pageSize!''}" totalPage="${(comments.page).totalPage!''}" url="${context.contextPath}/topic/query/queryDetailData/${data.id}"> </@pageCode></div>
                        </div>
                    </#if>
                </#if>
            </#if>
            </ul>
            <div class="layui-form layui-form-pane">
                <form action="${context.contextPath}/comment/management/addData/${data.id}" method="post">
                    <div class="layui-form-item layui-form-text">
                        <a name="comment"></a>
                        <div class="layui-input-block">
                            <textarea id="L_content" name="content" required lay-verify="required" placeholder="请输入回复内容"
                                      class="layui-textarea fly-editor" style="height: 150px;"></textarea>
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <button class="layui-btn" lay-filter="btnSubmit" lay-submit>提交回复</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="${context.contextPath}/assets/js/views/topic/topic_detail.js"></script>
<#include "/_include/right.ftl">
</div>
