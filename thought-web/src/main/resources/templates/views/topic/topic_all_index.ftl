<title>iartsian-thought</title>
<div class="layui-row layui-col-space15">
    <div class="layui-col-md8">
    <#include "/topic/topic_list_data.ftl">
    <#if (_topicPageData.page) ?? >
        <div style="text-align: center">
            <div class="laypage-main"><@pageCode currPage="${(_topicPageData.page).currPage!''}" pageSize="${(_topicPageData.page).pageSize!''}" totalPage="${(_topicPageData.page).totalPage!''}" url="${context.contextPath}/topic/query/queryPageData/${_categoryKey}/${_statusKey}"> </@pageCode></div>
        </div>
    </#if>
    </div>
<#include "/_include/right.ftl">
</div>