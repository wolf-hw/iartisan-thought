<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><sitemesh:write property='title'/></title>
    <#include "/_include/head.ftl"/>
</head>
<body>
<#include "/_include/nav/nav_first.ftl"/>
<#include "/_include/nav/nav_second.ftl"/>
<div class="layui-container">
    <sitemesh:write property='body'/>
</div>
<#include "/_include/footer.ftl">
</body>
</html>