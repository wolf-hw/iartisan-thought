<title>用户中心</title>
<div class="layui-tab layui-tab-brief" lay-filter="user">
    <ul class="layui-tab-title" id="LAY_mine">
        <li data-type="mine-jie" lay-id="index" class="layui-this" id="myCollection">我发的帖</li>
        <li data-type="collection" data-url="/collection/find/" lay-id="collection" id="collection">我收藏的帖</li>
    </ul>
    <div class="layui-tab-content" style="padding: 20px 0;">
        <div class="layui-tab-item layui-show">
            <table id="topicTableList"></table>
        </div>
    </div>
</div>
<script type="text/html" id="switchCollect">
    <!-- 这里的 checked 的状态只是演示 -->
    <a class='layui-btn layui-btn-xs unCollect' href='javascript:void(0);' value="{{d.id}}">取消收藏</a>
</script>
<script type="text/javascript" src="${context.contextPath}/assets/js/views/my/my_index.js"></script>