layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'router', 'md5'], function () {
    var $ = layui.jquery,
        form = layui.form,
        router = layui.router;

    form.on('submit(btnLogin)', function (data) {
        var action = $(data.form).attr('action');
        data.field["pass"] = $.md5(data.field["pass"]);
        router.post({
                url: action,
                data: data.field,
                success: function (res) {
                    location.href = res.message;
                }
            }
        );
        return false;
    });
});