//一般直接写在一个js文件中
layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'router'], function () {
    var $ = layui.jquery, router = layui.router, layer = layui.layer;

    var urls = {
        deleteByCustId: "/userMessage/management/deleteByCustId"
    };
    //全部删除
    $("#btnDeleteAll").on("click", function () {
        layer.confirm('确定全部删除吗？', function (index) {
            layer.close(index);
            router.get({
                url: urls.deleteByCustId,
                success: function () {
                    location.reload();
                }
            })
        });
    });
});