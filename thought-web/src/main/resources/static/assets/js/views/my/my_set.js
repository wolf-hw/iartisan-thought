//一般直接写在一个js文件中
layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'fly'], function () {
    var $ = layui.jquery;
    var layer = layui.layer;
    var util = layui.util;
    var laytpl = layui.laytpl;
    var form = layui.form;
    var laypage = layui.laypage;
    var fly = layui.fly;
    var flow = layui.flow;
    var element = layui.element;
    var upload = layui.upload;

    var requestURL = {
        upload: "/oss/rest/upload",
        setAvatar: "/my/setAvatar"
    };

    if ($('.upload-img')[0]) {
        layui.use('upload', function (upload) {
            var avatarAdd = $('.avatar-add');
            upload.render({
                elem: '.upload-img',
                url: requestURL.upload,
                size: 5000000,
                before: function () {
                    avatarAdd.find('.loading').show();
                },
                done: function (res) {
                    if (res.code == '000000') {
                        $.post(requestURL.setAvatar, {
                            avatar: res.message
                        }, function () {
                            window.location.reload(true);
                        });
                    } else {
                        layer.msg(res.msg, {icon: 5});
                    }
                    avatarAdd.find('.loading').hide();
                },
                error: function () {
                    avatarAdd.find('.loading').hide();
                }
            });
        });
    }
});