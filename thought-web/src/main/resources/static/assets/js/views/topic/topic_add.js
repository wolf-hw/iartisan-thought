layui.config({
    base: "/assets/js/lib/"
}).use(['layer', 'form', 'fly', 'router'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        router = layui.router,
        fly = layui.fly;

    //加载编辑器
    fly.layEditor({
        elem: '.fly-editor'
    });

    var page = {
        requestURL: {},
        initialization: function () {
            this.bindUI();
        },
        bindUI: function () {
            page.logic.addData();
            page.logic.initSelect();
        },
        logic: {
            addData: function () {
                form.on('submit(btnSubmit)', function (data) {
                    var action = $(data.form).attr('action'), button = $(data.elem);
                    //内容转义
                    var content = data.field.content;
                    content = /^{html}/.test(content) ? content.replace(/^{html}/, '') : fly.content(content);
                    data.field.content = content;
                    router.post({
                        url: action,
                        data: data.field,
                        success: function (res) {
                            location.href = res.message
                        }
                    });
                    return false;
                });
            },
            initSelect: function () {
                form.on('select(column)', function (obj) {
                    var value = obj.value,
                        elemQuiz = $('#LAY_quiz'),
                        tips = {
                            tips: 1,
                            maxWidth: 250,
                            time: 10000
                        };
                    elemQuiz.addClass('layui-hide');
                    if (value === '0') {
                        layer.tips('下面的信息将便于您获得更好的答案', obj.othis, tips);
                        elemQuiz.removeClass('layui-hide');
                    } else if (value === '99') {
                        layer.tips('系统会对【分享】类型的帖子予以飞吻奖励，但我们需要审核，通过后方可展示', obj.othis, tips);
                    }
                });
            }
        }
    };
    page.initialization();
});