//一般直接写在一个js文件中
layui.config({
    base: "/assets/mods/"
}).use(['layer', 'form', 'fly'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        fly = layui.fly;

    //加载编辑器
    fly.layEditor({
        elem: '.fly-editor'
    });

    var requestURL = {
        accept: "/comment/management/accept"
    };

    gather = {}, dom = {jieda: $('#jieda'), content: $('#L_content'), jiedaCount: $('#jiedaCount')};

    $('.jieda-reply span').on('click', function () {
        var othis = $(this), type = othis.attr('type');
        gather.jiedaActive[type].call(this, othis.parents('li'));
    });
    //解答操作
    gather.jiedaActive = {
        reply: function (li) { //回复
            var val = dom.content.val();
            var aite = '@' + li.find('.fly-detail-user cite').text().replace(/\s/g, '');
            dom.content.focus()
            if (val.indexOf(aite) !== -1) return;
            dom.content.val(aite + ' ' + val);
        },
        accept: function (li) { //采纳
            var othis = $(this);
            layer.confirm('是否采纳该回答为最佳答案？', function (index) {
                layer.close(index);
                fly.json(requestURL.accept,
                    {commentId: li.data('id')},
                    function (res) {
                        if (res.code === '000000') {
                            $('.jieda-accept').remove();
                            li.addClass('jieda-daan');
                            li.find('.detail-about').append('<i class="iconfont icon-caina" title="最佳答案"></i>');
                            $("#topidIsEnd").html("已结");
                        } else {
                            layer.msg(res.msg);
                        }
                    });
            });
        }
    };
    //提交
    form.on('submit(btnSubmit)', function (data) {
        var action = $(data.form).attr('action'), button = $(data.elem);
        //内容转义
        var content = data.field.content;
        content = /^{html}/.test(content) ? content.replace(/^{html}/, '') : fly.content(content);
        data.field.content = content;
        fly.json(action, data.field, function (res) {
            var end = function () {
                if (res.message) {
                    location.href = res.message;
                } else {
                    fly.form[action || button.attr('key')](data.field, data.form);
                }
            };
            if (res.code == '000000') {
                button.attr('alert') ? layer.alert(res.msg, {icon: 1, time: 10 * 1000, end: end}) : end();
            }
        });
        return false;
    });
});