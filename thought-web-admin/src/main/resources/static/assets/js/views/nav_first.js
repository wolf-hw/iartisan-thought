layui.use(['util', 'element'], function () {
    var util = layui.util;
    //固定Bar
    util.fixbar({
        bar1: '&#xe642;',
        bgcolor: '#009688',
        click: function (type) {
            if (type === 'bar1') {
                location.href = '/topic/management/addDataPage';
            }
        }
    });
});