layui.config({
    base: "/assets/mods/"
}).use(['layer', 'form', 'fly', 'table'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        table = layui.table;


    var page = {
        requestURL: {
            queryMyPageData: "/topic/query/queryMyPageData"
        },
        initialization: function () {
            page.logic.initMyTopicTable();
            this.bindUI();
        },
        bindUI: function () {

        },
        logic: {
            initMyTopicTable: function () {
                table.render({
                    elem: '#myTopicTableList',
                    url: page.requestURL.queryMyPageData,
                    method: 'get',
                    cols: [[
                        {
                            field: 'title',
                            title: '帖子标题',
                            minWidth: 300,
                            templet: '<div><a href="/topic/query/queryDetailData/{{ d.id }}" target="_blank" class="layui-table-link">{{ d.title }}</a></div>'
                        },
                        {
                            field: 'isFine',
                            title: '加精',
                            width: 100,
                            align: 'center',
                            templet: '<div><span style="font-size: 12px;">{{#  if(d.isFine=== \'1\'){ }}' + '<span style="color: #FF5722;">加精</span>' + '{{#  } }} </span></div>'
                        },
                        {
                            field: 'isEnd',
                            title: '结贴',
                            width: 100,
                            align: 'center',
                            templet: '<div><span style="font-size: 12px;">{{#  if(d.isEnd=== \'1\'){ }}' + '<span style="color: #5FB878;">已结</span>' + '{{#  } }} </span></div>'
                        },
                        {
                            field: 'time',
                            title: '发表时间',
                            width: 120,
                            align: 'center',
                            templet: '<div>{{ layui.util.timeAgo(d.createTime, 1) }}</div>'
                        },
                        {
                            title: '数据',
                            width: 150,
                            templet: '<div><span style="font-size: 12px;">{{d.readCount}}阅/{{d.commentCount}}答</span></div>'
                        },
                        {
                            title: '操作', width: 100, templet: function (d) {
                            return d.accept == -1 ? '<a class="layui-btn layui-btn-xs" href="/jie/edit/' + d.id + '" target="_blank">编辑</a>' : ''
                        }
                        }
                    ]],
                    page: true,
                    skin: 'line',
                    limits: [10],
                    response: {
                        statusCode: "000000",
                        dataName: "dataList",
                        countName: "totalRecords"
                    },
                    request: {
                        pageName: 'currPage', //页码的参数名称，默认：page,
                        limitName: 'pageSize' //每页数据量的参数名，默认：limit
                    }
                });
            }
        }
    };
    page.initialization();
});