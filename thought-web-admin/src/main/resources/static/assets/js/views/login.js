layui.config({
    base: "/assets/mods/"
}).use(['layer', 'form', 'fly'], function () {
    var $ = layui.jquery,
        layer = layui.layer,
        form = layui.form,
        fly = layui.fly;

    //加载编辑器
    fly.layEditor({
        elem: '.fly-editor'
    });

    var page = {
        requestURL: {},
        initialization: function () {
            this.bindUI();
        },
        bindUI: function () {
            page.logic.login();
        },
        logic: {
            login: function () {
                form.on('submit(btnLogin)', function (data) {
                    var action = $(data.form).attr('action'), button = $(data.elem);
                    fly.json(action, data.field, function (res) {
                        var end = function () {
                            if (res.message) {
                                location.href = res.message;
                            } else {
                                fly.form[action || button.attr('key')](data.field, data.form);
                            }
                        };
                        if (res.code == '000000') {
                            button.attr('alert') ? layer.alert(res.msg, {icon: 1, time: 10 * 1000, end: end}) : end();
                        }
                    });
                    return false;
                });
            }
        }
    };
    page.initialization();
});