package org.iartisan.thought.server.service.bo;

/**
 * <p>
 * BO
 *
 * @author King
 * @since 2017/12/19
 */
public class CategoryBO {

    private String categoryKey;

    private String categoryName;

    public String getCategoryKey() {
        return categoryKey;
    }

    public void setCategoryKey(String categoryKey) {
        this.categoryKey = categoryKey;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }
}
