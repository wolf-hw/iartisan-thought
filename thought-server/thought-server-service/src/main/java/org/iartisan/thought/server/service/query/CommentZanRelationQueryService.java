package org.iartisan.thought.server.service.query;

import org.iartisan.runtime.support.BaseQueryServiceSupport;
import org.iartisan.thought.server.dbm.mapper.CommentZanRelationMapper;
import org.iartisan.thought.server.dbm.model.CommentZanRelationDO;
import org.iartisan.thought.server.service.bo.CommentZanRelationBO;
import org.springframework.stereotype.Service;

/**
 * <p>
 *
 * @author King
 * @since 2018/2/7
 */
@Service
public class CommentZanRelationQueryService extends BaseQueryServiceSupport<CommentZanRelationMapper, CommentZanRelationBO> {


    public boolean isZan(String commentId, String custId) {
        CommentZanRelationDO dbQuery = new CommentZanRelationDO();
        dbQuery.setCommentId(commentId);
        dbQuery.setCustomerId(custId);
        CommentZanRelationDO dbResult = baseMapper.selectOne(dbQuery);
        if (null != dbResult) {
            return true;
        }
        return false;
    }
}
