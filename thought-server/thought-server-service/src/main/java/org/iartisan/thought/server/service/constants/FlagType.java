package org.iartisan.thought.server.service.constants;

/**
 * <p>
 * 是 、否 flag
 *
 * @author King
 * @since 2018/1/4
 */
public enum FlagType {
    Y, N
}
