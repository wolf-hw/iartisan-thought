package org.iartisan.thought.server.service.constants;

/**
 * <p>
 * db 数据有效性字段
 *
 * @author King
 * @since 2018/1/3
 */
public enum DataStatus {
    E, D, M
}
