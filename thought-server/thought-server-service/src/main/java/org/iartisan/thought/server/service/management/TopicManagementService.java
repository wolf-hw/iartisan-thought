package org.iartisan.thought.server.service.management;

import org.iartisan.runtime.support.BaseManagementServiceSupport;
import org.iartisan.runtime.utils.UUIDUtil;
import org.iartisan.thought.server.dbm.mapper.TopicMapper;
import org.iartisan.thought.server.dbm.model.TopicDO;
import org.iartisan.thought.server.service.bo.TopicBO;
import org.iartisan.thought.server.service.constants.DataStatus;
import org.iartisan.thought.server.service.constants.FlagType;
import org.iartisan.thought.server.service.query.TopicQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 * topic management service
 *
 * @author King
 * @since 2017/12/14
 */
@Service
public class TopicManagementService extends BaseManagementServiceSupport<TopicMapper, TopicBO> {

    @Autowired
    private TopicQueryService topicQueryService;

    public void saveData(TopicBO topicBO) {
        TopicDO topicDO = new TopicDO();
        topicDO.setId(UUIDUtil.shortId());
        topicDO.setTitle(topicBO.getTitle());
        topicDO.setContent(topicBO.getContent());
        topicDO.setCreateTime(new Date());
        topicDO.setCategoryKey(topicBO.getCategoryKey());
        topicDO.setCustomerId(topicBO.getCustomerId());
        topicDO.setIsTop(FlagType.N.toString());
        topicDO.setIsEnd(FlagType.N.toString());
        topicDO.setIsFine(FlagType.N.toString());
        topicDO.setStatus(DataStatus.E.toString());
        baseMapper.insert(topicDO);
    }

    public void addCommentCount(String topicId, int count) {
        TopicDO dbResult = topicQueryService.getTopicByTopicId(topicId);
        dbResult.setCommentCount(dbResult.getCommentCount().intValue() + count);
        baseMapper.updateById(dbResult);
    }

    public void addReadCount(String topicId, int count) {
        TopicDO dbResult = topicQueryService.getTopicByTopicId(topicId);
        dbResult.setReadCount(dbResult.getReadCount().intValue() + count);
        baseMapper.updateById(dbResult);
    }

    public void setTopicEnd(String topicId) {
        TopicDO dbResult = topicQueryService.getTopicByTopicId(topicId);
        dbResult.setIsEnd(FlagType.Y.toString());
        baseMapper.updateById(dbResult);
    }

    public void deleteTopic(String topicId) {
        TopicDO dbResult = topicQueryService.getTopicByTopicId(topicId);
        dbResult.setStatus(DataStatus.D.name());
        baseMapper.updateById(dbResult);
    }

    public void setTopicFine(String topicId) {
        TopicDO dbResult = topicQueryService.getTopicByTopicId(topicId);
        if (dbResult.getIsFine().equals(FlagType.Y.name())) {
            dbResult.setIsFine(FlagType.N.name());
        } else {
            dbResult.setIsFine(FlagType.Y.name());
        }
        baseMapper.updateById(dbResult);
    }

    public void setTopicTop(String topicId) {
        TopicDO dbResult = topicQueryService.getTopicByTopicId(topicId);
        if (dbResult.getIsTop().equals(FlagType.Y.name())) {
            dbResult.setIsTop(FlagType.N.name());
        } else {
            dbResult.setIsTop(FlagType.Y.name());
        }
        baseMapper.updateById(dbResult);
    }
}
