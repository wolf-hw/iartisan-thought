package org.iartisan.thought.server.service.query;

import org.iartisan.runtime.support.BaseQueryServiceSupport;
import org.iartisan.thought.server.dbm.mapper.CustTopicCollectMapper;
import org.iartisan.thought.server.dbm.model.CustTopicCollectDO;
import org.iartisan.thought.server.service.bo.CustTopicCollectBO;
import org.iartisan.thought.server.service.constants.DataStatus;
import org.springframework.stereotype.Service;

/**
 * <p>
 * collect manage
 *
 * @author King
 * @since 2018/1/12
 */
@Service
public class CustTopicCollectQueryService extends BaseQueryServiceSupport<CustTopicCollectMapper, CustTopicCollectBO> {

    public CustTopicCollectDO queryCollect(String custId, String topicId) {
        CustTopicCollectDO entity = new CustTopicCollectDO();
        entity.setTopicId(topicId);
        entity.setCustomerId(custId);
        entity.setStatus(DataStatus.E.toString());
        CustTopicCollectDO dbResult = baseMapper.selectOne(entity);
        return dbResult;
    }

    public boolean isCollect(String custId, String topicId) {
        CustTopicCollectDO dbResult = this.queryCollect(custId, topicId);
        if (dbResult != null) {
            return true;
        }
        return false;
    }
}
