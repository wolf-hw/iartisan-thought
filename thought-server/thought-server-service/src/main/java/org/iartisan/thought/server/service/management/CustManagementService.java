package org.iartisan.thought.server.service.management;

import org.iartisan.runtime.utils.UUIDUtil;
import org.iartisan.thought.server.dbm.mapper.CustBaseMapper;
import org.iartisan.thought.server.dbm.mapper.CustCipMapper;
import org.iartisan.thought.server.dbm.model.CustBaseDO;
import org.iartisan.thought.server.dbm.model.CustCipDO;
import org.iartisan.thought.server.service.constants.CustType;
import org.iartisan.thought.server.service.constants.DataStatus;
import org.iartisan.thought.server.service.handler.MessageHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * <p>
 * cust query service
 *
 * @author King
 * @since 2017/12/15
 */
@Service
public class CustManagementService {

    @Autowired
    private CustBaseMapper custBaseMapper;

    @Autowired
    private CustCipMapper custCipMapper;

    @Autowired
    private MessageHandler messageHandler;

    @Transactional
    public void addCustCipData(String email, String nickName, String pwd) {
        String custId = UUIDUtil.shortId();
        CustBaseDO dbBaseInsert = new CustBaseDO();
        dbBaseInsert.setCustId(custId);
        dbBaseInsert.setCustAccount(email);
        dbBaseInsert.setCustType(CustType.cip.toString());
        dbBaseInsert.setCustPwd(pwd);
        dbBaseInsert.setCustStatus(DataStatus.E.toString());
        dbBaseInsert.setCreateTime(new Date());
        //添加到基表
        custBaseMapper.insert(dbBaseInsert);
        //添加到cip表
        CustCipDO dbCipInsert = new CustCipDO();
        dbCipInsert.setCustId(custId);
        dbCipInsert.setCustNickname(nickName);
        dbCipInsert.setCreateTime(new Date());
        dbCipInsert.setCipEmail(email);
        custCipMapper.insert(dbCipInsert);
        //发送欢迎消息
        messageHandler.sendWelcomeMsg(custId);
    }

    public void modifyAvatar(String custId, String avatar) {
        CustBaseDO dbModify = new CustBaseDO();
        dbModify.setCustId(custId);
        dbModify.setAvatar(avatar);
        custBaseMapper.updateById(dbModify);
    }

}
