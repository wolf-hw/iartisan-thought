package org.iartisan.thought.server.service.constants;

/**
 * <p>
 * cust type
 *
 * @author King
 * @since 2017/12/16
 */
public enum CustType {
    cip, admin
}
