CREATE TABLE `warm_channel` (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `CHANNEL_NAME` varchar(200) NOT NULL COMMENT '通道名称',
  `CHANNEL_LOCATION` varchar(48) NOT NULL COMMENT '贴子地址',
  `CHANNEL_TYPE` varchar(10) DEFAULT NULL COMMENT '通道类型',
  `CHANNEL_WEIGHT` int(11) DEFAULT '0' COMMENT '通道排序权重',
  `CREATE_TIME` datetime DEFAULT NULL COMMENT '创建时间',
  `UPDATE_TIME` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `unique_channel_location` (`CHANNEL_LOCATION`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='温馨通道';