package org.iartisan.thought.server.api.req;

/**
 * @author King
 * @since 2018/1/16
 */
public class CustModifyReq extends BaseCustReq {

    private String avatar;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
