package org.iartisan.thought.server.api.management;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.req.BaseTopicReq;

/**
 * <p>
 * 温馨通道管理
 *
 * @author King
 * @since 2018/1/30
 */
public interface WarmChannelManagementFacade {

    /**
     * 添加温馨通道
     *
     * @param req
     * @return
     */
    BaseRes setWarmChannel(BaseTopicReq req);
}
