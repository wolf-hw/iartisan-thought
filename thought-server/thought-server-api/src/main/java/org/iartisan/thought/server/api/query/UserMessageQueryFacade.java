package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.res.UserMessageBean;

/**
 * <p>
 * 消息查询
 *
 * @author King
 * @since 2018/2/6
 */
public interface UserMessageQueryFacade {

    /**
     * 查询未读消息
     *
     * @param req
     * @return
     */
    BaseOneRes<Integer> getUnreadMsgCount(BaseCustReq req);

    /**
     * 查询未读消息
     *
     * @param req
     * @return
     */
    BaseListRes<UserMessageBean> getUnreadMsgs(BaseCustReq req);

}
