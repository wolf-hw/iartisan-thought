package org.iartisan.thought.server.api.query;


import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.res.CommentBean;

/**
 * <p>
 * comment management
 *
 * @author King
 * @since 2017/12/15
 */
public interface CommentQueryFacade {

    /**
     * 分页查询
     * comment
     *
     * @param req
     * @return
     */
    BasePageRes<CommentBean> getCommentPageData(BaseTopicReq req, Page page);

}
