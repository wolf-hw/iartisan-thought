package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.thought.server.api.res.CategoryBean;

/**
 * <p>
 * basic query facade
 *
 * @author King
 * @since 2017/12/19
 */
public interface BasicQueryFacade {

    BaseListRes<CategoryBean> getCategoryList(BaseReq req);

}
