package org.iartisan.thought.server.api.management;


import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.server.api.req.CustModifyReq;

/**
 * <p>
 * cust query
 *
 * @author King
 * @since 2017/12/15
 */
public interface CustManagementFacade {

    /**
     * 添加会员 数据
     *
     * @param req
     * @return
     */
    BaseRes addCustData(CustCipAddReq req);

    BaseRes setCustAvatar(CustModifyReq req);

}
