package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.res.BaseCheckRes;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.api.res.BaseOneRes;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.MyTopicQueryReq;
import org.iartisan.thought.server.api.req.TopicCollectReq;
import org.iartisan.thought.server.api.req.TopicQueryReq;
import org.iartisan.thought.server.api.res.TopicBean;

/**
 * <p>
 * 贴子查询服务
 *
 * @author King
 * @since 2017/12/8
 */
public interface TopicQueryFacade {

    /**
     * 查询所有贴子
     *
     * @param req
     * @return
     */
    BasePageRes<TopicBean> getTopicPageData(TopicQueryReq req, Page page);

    /**
     * 查询我发的贴
     *
     * @param req
     * @param page
     * @return
     */
    BasePageRes<TopicBean> getMyTopicPageData(MyTopicQueryReq req, Page page);

    /**
     * 查询置顶贴
     *
     * @param req
     * @return
     */
    BaseListRes<TopicBean> getTopTopicList(BaseReq req);

    /**
     * detail
     *
     * @param req
     * @return
     */
    BaseOneRes<TopicBean> getTopicDetailById(BaseTopicReq req);

    /**
     * 最热贴
     *
     * @param req
     * @return
     */
    BaseListRes<TopicBean> getHotTopicList(BaseReq req);

}
