package org.iartisan.thought.server.api.req;

import org.iartisan.runtime.api.base.BaseReq;

/**
 * <p>
 * query req
 *
 * @author King
 * @since 2017/12/22
 */
public class MyTopicQueryReq extends BaseReq {

    private String custId;

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
}
