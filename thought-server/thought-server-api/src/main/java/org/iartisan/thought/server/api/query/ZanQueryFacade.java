package org.iartisan.thought.server.api.query;

import org.iartisan.runtime.api.res.BaseCheckRes;
import org.iartisan.thought.server.api.req.CommentZanReq;

/**
 * <p>
 * 赞查询接口
 *
 * @author King
 * @since 2018/2/8
 */
public interface ZanQueryFacade {

    //是否被赞过
    BaseCheckRes isZaned(CommentZanReq req);
}
