package org.iartisan.thought.server.api.res;

import java.io.Serializable;

/**
 * <p>
 * cust bean
 *
 * @author King
 * @since 2017/12/15
 */
public class CustBean implements Serializable {

    private String custId;

    private String custAccount;

    private String custType;

    private String custPwd;

    private String custNickName;

    private String avatar;

    private String custGender;

    public String getCustAccount() {
        return custAccount;
    }

    public void setCustAccount(String custAccount) {
        this.custAccount = custAccount;
    }

    public String getCustType() {
        return custType;
    }

    public void setCustType(String custType) {
        this.custType = custType;
    }

    public String getCustPwd() {
        return custPwd;
    }

    public void setCustPwd(String custPwd) {
        this.custPwd = custPwd;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustNickName() {
        return custNickName;
    }

    public void setCustNickName(String custNickName) {
        this.custNickName = custNickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getCustGender() {
        return custGender;
    }

    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }
}
