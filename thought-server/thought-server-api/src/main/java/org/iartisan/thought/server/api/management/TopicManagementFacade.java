package org.iartisan.thought.server.api.management;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.TopicAddReq;
import org.iartisan.thought.server.api.req.TopicCollectReq;

/**
 * <p>
 * 贴子管理服务
 *
 * @author King
 * @since 2017/12/8
 */
public interface TopicManagementFacade {

    /**
     * 添加贴子
     *
     * @param req
     * @return
     */
    BaseRes addTopic(TopicAddReq req);

    /**
     * 增加文章阅读数
     *
     * @param req
     * @return
     */
    BaseRes addReadcount(BaseTopicReq req);

    /**
     * 贴子收藏功能
     *
     * @param req
     * @return
     */
    BaseRes addCollectTopic(TopicCollectReq req);

    /**
     * 删除贴子
     *
     * @param req
     * @return
     */
    BaseRes deleteTopic(BaseTopicReq req);

    /**
     * 置顶贴子
     *
     * @param req
     * @return
     */
    BaseRes setTopicTop(BaseTopicReq req);

    /**
     * 加精贴子
     *
     * @param req
     * @return
     */
    BaseRes setTopicFine(BaseTopicReq req);
}
