package org.iartisan.thought.server.api.req;

import java.io.Serializable;

/**
 * <p>
 * topic collect
 *
 * @author King
 * @since 2018/1/5
 */
public class TopicCollectReq implements Serializable {

    private String topicId;

    private String custId;

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }
}
