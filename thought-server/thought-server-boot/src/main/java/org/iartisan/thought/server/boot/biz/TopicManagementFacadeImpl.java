package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.management.TopicManagementFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.req.TopicAddReq;
import org.iartisan.thought.server.api.req.TopicCollectReq;
import org.iartisan.thought.server.service.bo.CustTopicCollectBO;
import org.iartisan.thought.server.service.management.CustTopicCollectManagementService;
import org.iartisan.thought.server.service.management.TopicManagementService;
import org.iartisan.thought.server.service.bo.TopicBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * topic management implements
 *
 * @author King
 * @since 2017/12/14
 */
@Service("topicManagementFacade")
public class TopicManagementFacadeImpl implements TopicManagementFacade {

    @Autowired
    private TopicManagementService topicManagementService;

    @Autowired
    private CustTopicCollectManagementService custTopicCollectManagementService;

    @Override
    public BaseRes addTopic(TopicAddReq req) {
        BaseRes res = new BaseRes();
        TopicBO topicBO = new TopicBO();
        topicBO.setCustomerId(req.getCustId());
        topicBO.setTitle(req.getTitle());
        topicBO.setContent(req.getContent());
        topicBO.setCategoryKey(req.getCategoryKey());
        topicManagementService.saveData(topicBO);
        return res;
    }

    @Override
    public BaseRes addReadcount(BaseTopicReq req) {
        BaseRes res = new BaseRes();
        topicManagementService.addReadCount(req.getTopicId(), 1);
        return res;
    }

    @Override
    public BaseRes addCollectTopic(TopicCollectReq req) {
        BaseRes res = new BaseRes();
        CustTopicCollectBO t = new CustTopicCollectBO();
        t.setCustomerId(req.getCustId());
        t.setTopicId(req.getTopicId());
        custTopicCollectManagementService.collect(t);
        return res;
    }

    @Override
    public BaseRes deleteTopic(BaseTopicReq req) {
        BaseRes res = new BaseRes();
        topicManagementService.deleteTopic(req.getTopicId());
        return res;
    }

    @Override
    public BaseRes setTopicTop(BaseTopicReq req) {
        BaseRes res = new BaseRes();
        topicManagementService.setTopicTop(req.getTopicId());
        return res;
    }

    @Override
    public BaseRes setTopicFine(BaseTopicReq req) {
        BaseRes res = new BaseRes();
        topicManagementService.setTopicFine(req.getTopicId());
        return res;
    }
}
