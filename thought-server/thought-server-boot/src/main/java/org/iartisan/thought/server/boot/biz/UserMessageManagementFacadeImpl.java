package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.management.UserMessageManagementFacade;
import org.iartisan.thought.server.api.req.BaseCustReq;
import org.iartisan.thought.server.api.req.BaseIdReq;
import org.iartisan.thought.server.service.management.UserMessageManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author King
 * @since 2018/2/7
 */
@Service("userMessageManagementFacade")
public class UserMessageManagementFacadeImpl implements UserMessageManagementFacade {

    @Autowired
    private UserMessageManagementService userMessageManagementService;

    @Override
    public BaseRes deleteByCustId(BaseCustReq req) {
        BaseRes res = new BaseRes();
        userMessageManagementService.deleteDataByCustId(req.getCustId());
        return res;
    }

    @Override
    public BaseRes deleteById(BaseIdReq req) {
        BaseRes res = new BaseRes();
        userMessageManagementService.deleteDataById(req.getId());
        return res;
    }

}
