package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.runtime.exception.NotAllowedException;
import org.iartisan.thought.server.api.management.CommentManagementFacade;
import org.iartisan.thought.server.api.req.BaseCommentReq;
import org.iartisan.thought.server.api.req.CommentAddReq;
import org.iartisan.thought.server.api.req.CommentZanReq;
import org.iartisan.thought.server.service.bo.CommentBO;
import org.iartisan.thought.server.service.management.CommentManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * comment management
 *
 * @author King
 * @since 2018/1/3
 */
@Service("commentManagementFacade")
public class CommentManagementFacadeImpl implements CommentManagementFacade {

    @Autowired
    private CommentManagementService commentManagementService;


    @Override
    public BaseRes addCommentData(CommentAddReq req) {
        BaseRes res = new BaseRes();
        CommentBO commentBO = new CommentBO();
        commentBO.setTopicId(req.getTopicId());
        commentBO.setCustomerId(req.getCustId());
        commentBO.setContent(req.getContent());
        commentManagementService.saveData(commentBO);
        return res;
    }

    @Override
    public BaseRes accept(BaseCommentReq req) {
        BaseRes res = new BaseRes();
        commentManagementService.accept(req.getCommentId());
        return res;
    }

    @Override
    public BaseRes addZan(CommentZanReq req) {
        BaseRes res = new BaseRes();
        try {
            commentManagementService.addZan(req.getCommentId(), req.getCustId());
        } catch (NotAllowedException e) {
            res.setNotAllowedErrorRes(e.getMessage());
        }
        return res;
    }
}
