package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.res.BaseCheckRes;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.api.query.CustTopicCollectQueryFacade;
import org.iartisan.thought.server.api.req.TopicCollectReq;
import org.iartisan.thought.server.api.res.TopicBean;
import org.iartisan.thought.server.service.bo.CustTopicCollectBO;
import org.iartisan.thought.server.service.bo.extend.TopicUserBO;
import org.iartisan.thought.server.service.query.CustTopicCollectQueryService;
import org.iartisan.thought.server.service.query.TopicQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust topic collect query facade implements
 *
 * @author King
 * @since 2018/1/12
 */
@Service("custTopicCollectFacade")
public class CustTopicCollectFacadeImpl implements CustTopicCollectQueryFacade {

    @Autowired
    private CustTopicCollectQueryService custTopicCollectQueryService;

    @Autowired
    private TopicQueryService topicQueryService;

    @Override
    public BaseCheckRes checkTopicCollect(TopicCollectReq req) {
        BaseCheckRes res = new BaseCheckRes();
        boolean result = custTopicCollectQueryService.isCollect(req.getCustId(), req.getTopicId());
        res.setResult(result);
        return res;
    }

    @Override
    public BasePageRes<TopicBean> getMyCollectTopicPageData(TopicCollectReq req, Page page) {
        BasePageRes<TopicBean> res = new BasePageRes<>();
        CustTopicCollectBO t = new CustTopicCollectBO();
        t.setCustomerId(req.getCustId());
        PageWrapper<CustTopicCollectBO> collectPage = custTopicCollectQueryService.getPageData(page, t);
        PageWrapper<TopicBean> pageWrapper = BeanUtil.copyPage(collectPage, TopicBean.class);
        for (TopicBean topicBean : pageWrapper.getDataList()) {
            //查询贴子信息
            TopicUserBO topicBO = topicQueryService.getTopicUserById(topicBean.getTopicId());
            topicBean.setTitle(topicBO.getTitle());
            topicBean.setStatus(topicBO.getStatus());
            topicBean.setTopicId(topicBO.getId());
            topicBean.setCreateTime(topicBO.getCreateTime());
            topicBean.setId(topicBean.getTopicId());
        }
        res.setDataPage(pageWrapper);
        return res;
    }
}
