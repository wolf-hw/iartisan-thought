package org.iartisan.thought.server.boot;

import org.iartisan.runtime.env.EnvPropertiesLoader;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import java.io.IOException;
import java.util.Properties;

/**
 * <p>
 * service 启动类
 *
 * @author King
 * @since 2017/10/17
 */
@SpringBootApplication
@ComponentScan(basePackages = {"org.iartisan.thought.server"})
@MapperScan(basePackages = {"org.iartisan.thought.server.dbm.mapper"})
public class ServerBoot {
    public static void main(String[] args) throws IOException {
        Properties properties = EnvPropertiesLoader.loadFile();
        SpringApplication application = new SpringApplication(ServerBoot.class);
        application.setDefaultProperties(properties);
        application.run(args);
    }
}
