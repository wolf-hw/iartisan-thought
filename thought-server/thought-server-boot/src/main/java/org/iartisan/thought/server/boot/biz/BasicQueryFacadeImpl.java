package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.api.query.BasicQueryFacade;
import org.iartisan.thought.server.api.res.CategoryBean;
import org.iartisan.thought.server.service.bo.CategoryBO;
import org.iartisan.thought.server.service.query.BasicQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * basic query facade implements
 *
 * @author King
 * @since 2017/12/19
 */
@Service("basicQueryFacade")
public class BasicQueryFacadeImpl implements BasicQueryFacade {

    @Autowired
    private BasicQueryService basicQueryService;

    @Override
    public BaseListRes<CategoryBean> getCategoryList(BaseReq req) {
        BaseListRes<CategoryBean> res = new BaseListRes<>();
        List<CategoryBO> list = basicQueryService.getCategories();
        res.setDataList(BeanUtil.copyList(list,CategoryBean.class));
        return res;
    }
}
