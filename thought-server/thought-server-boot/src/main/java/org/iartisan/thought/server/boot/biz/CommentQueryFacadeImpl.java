package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.bean.Page;
import org.iartisan.runtime.bean.PageWrapper;
import org.iartisan.runtime.api.res.BasePageRes;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.api.query.CommentQueryFacade;
import org.iartisan.thought.server.api.req.BaseTopicReq;
import org.iartisan.thought.server.api.res.CommentBean;
import org.iartisan.thought.server.service.bo.CommentBO;
import org.iartisan.thought.server.service.bo.CustBO;
import org.iartisan.thought.server.service.query.CommentQueryService;
import org.iartisan.thought.server.service.query.CustQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * comment query
 *
 * @author King
 * @since 2018/1/4
 */
@Service("commentQueryFacade")
public class CommentQueryFacadeImpl implements CommentQueryFacade {

    @Autowired
    private CommentQueryService commentQueryService;

    @Autowired
    private CustQueryService custQueryService;

    @Override
    public BasePageRes<CommentBean> getCommentPageData(BaseTopicReq req, Page page) {
        BasePageRes<CommentBean> res = new BasePageRes<>();
        CommentBO commentBO = new CommentBO();
        commentBO.setTopicId(req.getTopicId());
        PageWrapper<CommentBO> dbResult = commentQueryService.getEffectivePageData(page, commentBO);
        res.setDataPage(BeanUtil.copyPage(dbResult, CommentBean.class));
        //查询回贴用户信息
        for (CommentBean commentBean : res.getDataPage().getDataList()) {
            CustBO custBO = custQueryService.getCustInfoById(commentBean.getCustomerId());
            commentBean.setAvatar(custBO.getAvatar());
            commentBean.setCustomerId(custBO.getCustId());
            commentBean.setCustNickName(custBO.getCustNickName());
        }
        return res;
    }

}
