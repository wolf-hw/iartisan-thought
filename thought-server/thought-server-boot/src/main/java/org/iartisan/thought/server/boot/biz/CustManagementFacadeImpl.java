package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.base.BaseRes;
import org.iartisan.thought.server.api.management.CustManagementFacade;
import org.iartisan.thought.server.api.req.CustCipAddReq;
import org.iartisan.thought.server.api.req.CustModifyReq;
import org.iartisan.thought.server.service.management.CustManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * cust management facade impl
 *
 * @author King
 * @since 2017/12/16
 */
@Service("custManagementFacade")
public class CustManagementFacadeImpl implements CustManagementFacade {

    @Autowired
    private CustManagementService custManagementService;

    @Override
    public BaseRes addCustData(CustCipAddReq req) {
        BaseRes res = new BaseRes();
        custManagementService.addCustCipData(req.getCustEmail(), req.getCustNickname(), req.getCustPwd());
        return res;
    }

    @Override
    public BaseRes setCustAvatar(CustModifyReq req) {
        BaseRes res = new BaseRes();
        custManagementService.modifyAvatar(req.getCustId(), req.getAvatar());
        return res;
    }
}
