package org.iartisan.thought.server.boot.aspect;


import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.iartisan.runtime.api.aspect.BizAspect;
import org.springframework.stereotype.Component;

/**
 * <p>
 * 切面
 *
 * @author King
 * @since 2018/1/30
 */
@Component
@Aspect
public class ThoughtApiAspect extends BizAspect {

    @Pointcut("execution(* org.iartisan.thought.server.boot.biz.*.*(..))")
    public void doAspect() {

    }
}
