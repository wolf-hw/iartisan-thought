package org.iartisan.thought.server.boot.biz;

import org.iartisan.runtime.api.base.BaseReq;
import org.iartisan.runtime.api.res.BaseListRes;
import org.iartisan.runtime.utils.BeanUtil;
import org.iartisan.thought.server.api.query.WarmChannelQueryFacade;
import org.iartisan.thought.server.api.res.WarmChannelBean;
import org.iartisan.thought.server.service.bo.WarmChannelBO;
import org.iartisan.thought.server.service.query.WarmChannelQueryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * @author King
 * @since 2018/1/30
 */
@Service("warmChannelQueryFacade")
public class WarmChannelQueryFacadeImpl implements WarmChannelQueryFacade {


    @Autowired
    private WarmChannelQueryService warmChannelQueryService;

    @Override
    public BaseListRes<WarmChannelBean> getWarmChannels(BaseReq req) {
        BaseListRes<WarmChannelBean> res = new BaseListRes<>();
        List<WarmChannelBO> dataList = warmChannelQueryService.getAllData();
        res.setDataList(BeanUtil.copyList(dataList, WarmChannelBean.class));
        return res;
    }

}
