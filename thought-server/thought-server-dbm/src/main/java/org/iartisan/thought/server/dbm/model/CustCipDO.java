package org.iartisan.thought.server.dbm.model;

    import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.ibatis.type.Alias;
/**
* cust_cip 表模型
* @author King
*/
@Alias("custCip")
@TableName(value = "cust_cip")
public class CustCipDO {

        /**
        * 列名: CUST_ID
        * 备注: cust_id
        */
        @TableId("CUST_ID")
        private String custId;

        /**
        * 列名: CIP_EMAIL
        * 备注: EMAIL
        */
        @TableField("CIP_EMAIL")
        private String cipEmail;

        /**
        * 列名: CREATE_TIME
        * 备注: 创建时间
        */
        @TableField("CREATE_TIME")
        private Date createTime;

        /**
        * 列名: UPDATE_TIME
        * 备注: 修改时间
        */
        @TableField("UPDATE_TIME")
        private Date updateTime;

        /**
        * 列名: AVATAR
        * 备注: 头像路径
        */
        @TableField("AVATAR")
        private String avatar;

        /**
        * 列名: CUST_NICKNAME
        * 备注: 昵称
        */
        @TableField("CUST_NICKNAME")
        private String custNickname;


        public String getCustId(){
        return custId;
      }

        public void setCustId(String custId){
          this.custId = custId;
      }

        public String getCipEmail(){
        return cipEmail;
      }

        public void setCipEmail(String cipEmail){
          this.cipEmail = cipEmail;
      }

        public Date getCreateTime(){
        return createTime;
      }

        public void setCreateTime(Date createTime){
          this.createTime = createTime;
      }

        public Date getUpdateTime(){
        return updateTime;
      }

        public void setUpdateTime(Date updateTime){
          this.updateTime = updateTime;
      }

        public String getAvatar(){
        return avatar;
      }

        public void setAvatar(String avatar){
          this.avatar = avatar;
      }

        public String getCustNickname(){
        return custNickname;
      }

        public void setCustNickname(String custNickname){
          this.custNickname = custNickname;
      }

}