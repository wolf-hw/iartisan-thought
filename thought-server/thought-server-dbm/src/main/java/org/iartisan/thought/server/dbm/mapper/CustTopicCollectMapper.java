package org.iartisan.thought.server.dbm.mapper;


import org.apache.ibatis.annotations.CacheNamespace;
import org.iartisan.thought.server.dbm.model.CustTopicCollectDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * cust_topic_collect 表操作接口
 * @author King
 */
//@CacheNamespace
public interface CustTopicCollectMapper extends MybatisBaseMapper<CustTopicCollectDO>{

}