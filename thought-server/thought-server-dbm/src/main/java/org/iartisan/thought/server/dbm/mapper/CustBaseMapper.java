package org.iartisan.thought.server.dbm.mapper;


import org.iartisan.thought.server.dbm.model.CustBaseDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * cust_base 表操作接口
 * @author King
 */
public interface CustBaseMapper extends MybatisBaseMapper<CustBaseDO>{

}