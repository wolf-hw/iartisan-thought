package org.iartisan.thought.server.dbm.mapper;


import org.apache.ibatis.annotations.CacheNamespace;
import org.iartisan.thought.server.dbm.model.CommentDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * comment 表操作接口
 * @author King
 */
//@CacheNamespace
public interface CommentMapper extends MybatisBaseMapper<CommentDO>{

}