package org.iartisan.thought.server.dbm.model;

    import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.ibatis.type.Alias;
/**
* cust_base 表模型
* @author King
*/
@Alias("custBase")
@TableName(value = "cust_base")
public class CustBaseDO {

        /**
        * 列名: CUST_ID
        * 备注: cust_id
        */
        @TableId("CUST_ID")
        private String custId;

        /**
        * 列名: CUST_TYPE
        * 备注: 会员类型
        */
        @TableField("CUST_TYPE")
        private String custType;

        /**
        * 列名: CUST_ACCOUNT
        * 备注: 登录账号
        */
        @TableField("CUST_ACCOUNT")
        private String custAccount;

        /**
        * 列名: CUST_PWD
        * 备注: 密码
        */
        @TableField("CUST_PWD")
        private String custPwd;

        /**
        * 列名: CUST_STATUS
        * 备注: 状态 E:有效 D:删除
        */
        @TableField("CUST_STATUS")
        private String custStatus;

        /**
        * 列名: AVATAR
        * 备注: 头像路径
        */
        @TableField("AVATAR")
        private String avatar;

        /**
        * 列名: CUST_NICKNAME
        * 备注: 昵称
        */
        @TableField("CUST_NICKNAME")
        private String custNickname;

        @TableField("CUST_GENDER")
        private String custGender;

        /**
        * 列名: CREATE_TIME
        * 备注: 创建时间
        */
        @TableField("CREATE_TIME")
        private Date createTime;

        /**
        * 列名: UPDATE_TIME
        * 备注: 修改时间
        */
        @TableField("UPDATE_TIME")
        private Date updateTime;


        public String getCustId(){
        return custId;
      }

        public void setCustId(String custId){
          this.custId = custId;
      }

        public String getCustType(){
        return custType;
      }

        public void setCustType(String custType){
          this.custType = custType;
      }

        public String getCustAccount(){
        return custAccount;
      }

        public void setCustAccount(String custAccount){
          this.custAccount = custAccount;
      }

        public String getCustPwd(){
        return custPwd;
      }

        public void setCustPwd(String custPwd){
          this.custPwd = custPwd;
      }

        public String getCustStatus(){
        return custStatus;
      }

        public void setCustStatus(String custStatus){
          this.custStatus = custStatus;
      }

        public String getAvatar(){
        return avatar;
      }

        public void setAvatar(String avatar){
          this.avatar = avatar;
      }

        public String getCustNickname(){
        return custNickname;
      }

        public void setCustNickname(String custNickname){
          this.custNickname = custNickname;
      }

        public Date getCreateTime(){
        return createTime;
      }

        public void setCreateTime(Date createTime){
          this.createTime = createTime;
      }

        public Date getUpdateTime(){
        return updateTime;
      }

        public void setUpdateTime(Date updateTime){
          this.updateTime = updateTime;
      }

    public String getCustGender() {
        return custGender;
    }

    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }
}