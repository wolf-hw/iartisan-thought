package org.iartisan.thought.server.dbm.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.apache.ibatis.type.Alias;

/**
 * comment_zan_relation 表模型
 *
 * @author King
 */
@Alias("commentZanRelation")
@TableName(value = "comment_zan_relation")
public class CommentZanRelationDO {

    /**
     * 列名: ID
     * 备注: 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 列名: COMMENT_ID
     * 备注: COMMENT_ID
     */
    @TableField("COMMENT_ID")
    private String commentId;

    /**
     * 列名: CUSTOMER_ID
     * 备注: ZAN人ID
     */
    @TableField("CUSTOMER_ID")
    private String customerId;

    /**
     * 列名: CREATE_TIME
     * 备注: 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCommentId() {
        return commentId;
    }

    public void setCommentId(String commentId) {
        this.commentId = commentId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}