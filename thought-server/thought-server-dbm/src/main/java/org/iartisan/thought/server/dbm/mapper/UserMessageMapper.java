package org.iartisan.thought.server.dbm.mapper;


import org.iartisan.thought.server.dbm.model.UserMessageDO;
import org.iartisan.runtime.jdbc.MybatisBaseMapper;
/**
 * user_message 表操作接口
 * @author King
 */
public interface UserMessageMapper extends MybatisBaseMapper<UserMessageDO>{

}