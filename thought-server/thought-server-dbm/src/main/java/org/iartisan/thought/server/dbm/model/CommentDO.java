package org.iartisan.thought.server.dbm.model;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import org.apache.ibatis.type.Alias;

/**
 * comment 表模型
 *
 * @author King
 */
@Alias("comment")
@TableName(value = "comment")
public class CommentDO implements Serializable {

    /**
     * 列名: ID
     * 备注: 主键
     */
    @TableId("ID")
    private String id;

    /**
     * 列名: TOPIC_ID
     * 备注: 贴子ID
     */
    @TableField("TOPIC_ID")
    private String topicId;

    /**
     * 列名: CUSTOMER_ID
     * 备注: 回复人ID
     */
    @TableField("CUSTOMER_ID")
    private String customerId;

    /**
     * 列名: CONTENT
     * 备注: 内容
     */
    @TableField("CONTENT")
    private String content;

    /**
     * 列名: STATUS
     * 备注: 状态 E:有效 D:删除
     */
    @TableField("STATUS")
    private String status;

    /**
     * 列名: CREATE_TIME
     * 备注: 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 列名: UPDATE_TIME
     * 备注: 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;

    @TableField("IS_ACCEPT")
    private String isAccept;

    @TableField("ZAN_COUNT")
    private Integer zanCount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTopicId() {
        return topicId;
    }

    public void setTopicId(String topicId) {
        this.topicId = topicId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getIsAccept() {
        return isAccept;
    }

    public void setIsAccept(String isAccept) {
        this.isAccept = isAccept;
    }

    public Integer getZanCount() {
        return zanCount;
    }

    public void setZanCount(Integer zanCount) {
        this.zanCount = zanCount;
    }
}