package org.iartisan.thought.server.dbm.model;

import java.util.Date;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import org.apache.ibatis.type.Alias;

/**
 * warm_channel 表模型
 *
 * @author King
 */
@Alias("warmChannel")
@TableName(value = "warm_channel")
public class WarmChannelDO {

    /**
     * 列名: ID
     * 备注: 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 列名: CHANNEL_NAME
     * 备注: 通道名称
     */
    @TableField("CHANNEL_NAME")
    private String channelName;

    /**
     * 列名: CHANNEL_LOCATION
     * 备注: 贴子地址
     */
    @TableField("CHANNEL_LOCATION")
    private String channelLocation;

    /**
     * 列名: CHANNEL_TYPE
     * 备注: 通道类型
     */
    @TableField("CHANNEL_TYPE")
    private String channelType;

    /**
     * 列名: CHANNEL_WEIGHT
     * 备注: 通道排序权重
     */
    @TableField("CHANNEL_WEIGHT")
    private Integer channelWeight;

    /**
     * 列名: CREATE_TIME
     * 备注: 创建时间
     */
    @TableField("CREATE_TIME")
    private Date createTime;

    /**
     * 列名: UPDATE_TIME
     * 备注: 更新时间
     */
    @TableField("UPDATE_TIME")
    private Date updateTime;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getChannelLocation() {
        return channelLocation;
    }

    public void setChannelLocation(String channelLocation) {
        this.channelLocation = channelLocation;
    }

    public String getChannelType() {
        return channelType;
    }

    public void setChannelType(String channelType) {
        this.channelType = channelType;
    }

    public Integer getChannelWeight() {
        return channelWeight;
    }

    public void setChannelWeight(Integer channelWeight) {
        this.channelWeight = channelWeight;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

}