package org.iartisan.thought.server.dbm.config;

import org.iartisan.runtime.jdbc.MyBatisConfig;
import org.springframework.context.annotation.Configuration;

/**
 * <p>
 *
 * @author King
 * @since 2017/10/19
 */
@Configuration
public class PigeonMyBatisConfig extends MyBatisConfig {

}
